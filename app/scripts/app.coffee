"use strict"
angular.module("yeoman", ['ui.router'])

.config ($stateProvider, $urlRouterProvider) ->
  $urlRouterProvider.otherwise '/'
  $stateProvider
    .state 'main',
      url: '/'
      templateUrl: "views/main.html"
      controller: "MainCtrl"
    .state 'test',
      url: '/test'
      templateUrl: "views/test.html"
      controller: "TestCtrl"
