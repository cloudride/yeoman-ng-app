"use strict"
angular.module("yeoman")

.controller 'TestCtrl', ($scope) ->
  $scope.clock = now: new Date()
  updateClock = () ->
    $scope.clock.now = new Date()
  setInterval ->
    $scope.$apply updateClock
  , 1000
  updateClock()

.controller 'ParentCtrl', ($scope) ->
  $scope.person = greeted: false

.controller 'ChildCtrl', ($scope) ->
  $scope.sayHello = () ->
    $scope.person.name = "Ari Lerner"
    $scope.person.greeted = true

.controller 'ParseCtrl', ($scope, $parse) ->
  $scope.$watch 'expr', (newVal, oldVal, scope) ->
    if newVal isnt oldVal
      # let's setup parse fun with the expression
      parseFun = $parse(newVal)
      # get the value of parsed expression
      $scope.parsedValue = parseFun(scope)
    # Perfect for expression testing; can try ternary operator for ParentCtrl person:
    # person.greeted ? 'firstRow' : 'nonFirstRow'

.controller 'FilterCtrl', ($scope) ->
  $scope.isCapitalized = (str) ->
    str[0] == str[0].toUpperCase()

.directive 'myDirective', () ->
  restrict: 'A'
  replace: true
  template: '<a href="{{myUrl}}">{{myLinkText}}</a>'
  scope: {
    myUrl: '@'
    myLinkText: '@'
  }
